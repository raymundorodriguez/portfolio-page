import { Injectable, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { BehaviorSubject } from 'rxjs';


interface CustomAlert {
  text: string;
  type: '' | 'success' | 'warning' | 'danger' | 'info';
  timeOut: number;
}


@Injectable({
  providedIn: 'root'
})
export class AlertService {


  private state_alert: BehaviorSubject<CustomAlert> = new BehaviorSubject<CustomAlert>({ text: 'Hola Mundo!', type: '', timeOut: 500 } as CustomAlert);
  private alert: HTMLDivElement = {} as HTMLDivElement;

  public alert$ = this.state_alert.asObservable();


  constructor(@Inject(DOCUMENT) private document: Document) { }


  setTextAlert(_alert: CustomAlert): void {
    this.state_alert.next(_alert);
  }


  show(): void {
    this.alert = this.document.getElementById('alert') as HTMLDivElement;
    this.alert.classList.add('open');

    setTimeout(() => {
      this.alert.classList.remove('open');
    }, this.state_alert.getValue().timeOut);
  }


  close(): void {
    this.alert = this.document.getElementById('alert') as HTMLDivElement;
    this.alert.classList.remove('open');
  }

}
