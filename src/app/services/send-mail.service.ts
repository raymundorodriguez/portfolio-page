import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class SendMailService {


  private readonly ENDPOINT = environment.api;


  constructor(private http: HttpClient, private translate: TranslateService) { }


  getUserData(): Observable<any> {
    let lang = this.translate.currentLang !== undefined ? this.translate.currentLang : "es-MX";

    return this.http.get(`${this.ENDPOINT}/user/get-user?lang=${lang}`)
      .pipe(
        map((response: any) => {
          let { data } = response;
          const { langs, ...user } = data;
          return {
            ...user
          };
        })
      )
  }


  downloadCV(email: string): Observable<any> {
    return this.http.post<any>(`${this.ENDPOINT}/user/download-cv`, { 'email': email });
  }


  sendMail(body: any): Observable<any> {
    return this.http.post<any>(`${this.ENDPOINT}/home/send-mail`, body);
  }


}