import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { SendMailService } from './send-mail.service';


@Injectable({
  providedIn: 'root'
})
export class UserService {


  private state = new BehaviorSubject<any | null>(null);
  public user$ = this.state.asObservable();


  constructor(private userService: SendMailService) { }


  getUser(): void {
    this.userService.getUserData().subscribe((response: any) => {
      this.state.next(response);
    });
  }

}
