import { BehaviorSubject } from 'rxjs';
import { Injectable, Inject } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { DOCUMENT } from '@angular/common';


@Injectable({
  providedIn: 'root'
})
export class ThemesService {


  constructor(
    @Inject(DOCUMENT) private document: Document,
    private translate: TranslateService,
  ) {
    this.setLang();
    this.setColorTheme('color-3');
  }


  setColorTheme(color: string): void {

    const headerEl = this.document.getElementsByTagName('head')[0];

    const existingLinkEl = this.document.getElementById('color-theme') as HTMLLinkElement;

    if (existingLinkEl) {
      existingLinkEl.href = `${color}.css`;
    } else {
      const newLinkEl = this.document.createElement('link');
      newLinkEl.id = 'color-theme';
      newLinkEl.rel = 'stylesheet';
      newLinkEl.href = `${color}.css`;
      headerEl.appendChild(newLinkEl);
    }
  }


  isDarkThemeFromUser(): boolean {
    const prefersDark = window.matchMedia("(prefers-color-scheme: dark)");
    return prefersDark.matches;
  }


  setDarkTheme(dark_mode: boolean): void {
    if (dark_mode) {
      document.body.classList.add('dark');
    } else {
      document.body.classList.remove('dark');
    }
  }


  private setLang(): void {
    const wn = window.navigator as any;
    let lang: string = wn.languages ? wn.languages[0] : 'es-MX';
    lang = lang || wn.language || wn.browserLanguage || wn.userLanguage;
    let simple_lang = lang.split('-', lang.length)[0];
    let isSpanish = simple_lang === 'es' ? 'es-MX' : 'en-US';
    this.translate.use(isSpanish)
  }


}
