import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CommentService {

  private readonly ENDPOINT = environment.api;

  constructor(private http: HttpClient) { }

  getComments(): Observable<Comment[]> {
    return this.http.get<Comment[]>(`${this.ENDPOINT}/user/comment`).pipe()
  }

  insertComment(comment: Comment): Observable<any> {
    return this.http.post<any>(`${this.ENDPOINT}/user/comment`, comment).pipe()
  }

}
