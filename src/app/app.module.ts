import { NgModule } from '@angular/core';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

/// COMPONENTS
import { MainContainerComponent } from './components/main-container/main-container.component';
import { AsideComponent } from './components/aside/aside.component';
import { MainContentComponent } from './components/main-content/main-content.component';
import { StyleSwitcherComponent } from './components/style-switcher/style-switcher.component';
import { TypingAnimationComponent } from './components/typing-animation/typing-animation.component';
import { AlertComponent } from './components/alert/alert.component';
import { CustomModalComponent } from './components/custom-modal/custom-modal.component';
import { SpinnerComponent } from './components/spinner/spinner.component';

/// PAGES
import { HomeComponent } from './pages/home/home.component';
import { AboutComponent } from './pages/about/about.component';
import { ServicesComponent } from './pages/services/services.component';
import { PortfolioComponent } from './pages/portfolio/portfolio.component';
import { ContactComponent } from './pages/contact/contact.component';

/// Libraries
import { ApiKeyInterceptor } from './injector/api-key.interceptor';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslationHttpLoader } from './internationalization/translation-http-loader';
import { LoadingInterceptor } from './injector/loading.interceptor';
import { WhatTheySayComponent } from './pages/what-they-say/what-they-say.component';
import { CommentComponent } from './pages/comment/comment.component';


export function HttpLoaderFactory(http: HttpClient) {
  return new TranslationHttpLoader(http);
}


// COMPONENTS
const COMPONENTS = [
  MainContainerComponent,
  AsideComponent,
  MainContentComponent,
  StyleSwitcherComponent,
  TypingAnimationComponent,
  AlertComponent,
  SpinnerComponent,
  CustomModalComponent,
];


// PAGES 
const PAGES = [
  HomeComponent,
  AboutComponent,
  ServicesComponent,
  PortfolioComponent,
  ContactComponent,
  WhatTheySayComponent,
  CommentComponent
];


@NgModule({
  declarations: [
    AppComponent,
    ...COMPONENTS,
    ...PAGES,
  ],
  imports: [
    FormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ApiKeyInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoadingInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
