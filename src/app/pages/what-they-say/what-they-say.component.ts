import { Component, OnInit } from '@angular/core';
import { CommentService } from 'src/app/services/comment.service';

@Component({
  selector: 'app-what-they-say',
  templateUrl: './what-they-say.component.html',
  styleUrls: ['./what-they-say.component.scss']
})
export class WhatTheySayComponent implements OnInit {

  testimonils: Array<any> = new Array<any>();
  _showNote: boolean = false;
  note: any | null;
  note_author: string | null = '';

  constructor(private comments: CommentService) { }

  ngOnInit(): void {
    this.getComments();
  }

  getComments(): void {
    this.comments.getComments().subscribe((data) => {
      this.testimonils = data;
    });
  }

  getUserInitials(userName: string): string {
    const names = userName?.split(' ');

    if (names?.length > 0) {
      let initials = names[0].substring(0, 1).toUpperCase();
      initials += names[names.length - 1].substring(0, 1).toUpperCase();
      return initials;
    }
    return '';
  }

  showNote(item: any) {
    this._showNote = true;
    this.note_author = item.name;
    this.note = {
      title: item.title,
      description: item.description
    }
  }

}
