import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WhatTheySayComponent } from './what-they-say.component';

describe('WhatTheySayComponent', () => {
  let component: WhatTheySayComponent;
  let fixture: ComponentFixture<WhatTheySayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WhatTheySayComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WhatTheySayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
