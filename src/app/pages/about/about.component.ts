import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { DataItem } from 'src/app/model/data-item.model';
import { UserService } from 'src/app/services/user.service';


@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit, OnDestroy {


  short_name: string = '';
  info: Array<DataItem> = new Array<any>();
  skills: Array<any> = new Array<any>();
  education: Array<any> = new Array<any>();
  experience: Array<any> = new Array<any>();
  listObservers$: Array<Subscription> = [];


  constructor(public user: UserService) { }


  ngOnInit(): void {
    const observerAbout$ = this.user.user$.subscribe((data: any) => {
      if (data !== null && data !== undefined) {
        let date: Date;

        this.short_name = data.short_name;
        this.info = data.info;
        this.info.forEach((item) => {

          if (item.label == 'about.birthday') {
            let res = this.setDate(item.value);
            date = res;
            item.value = res.toLocaleDateString();
          }

          if (item.label == 'about.age') {
            item.value = this.getAge(date);
          }
        });

        this.skills = data.skills;
        this.education = data.education;
        this.experience = data.experience;
      }
    })
    this.listObservers$ = [observerAbout$]
  }


  ngOnDestroy(): void {
    this.listObservers$.forEach(u => u.unsubscribe());
  }


  getPercent(value: number): string {
    return `${value.toString()}%`;
  }

  setDate(dateString: string): Date {
    let dateArray = dateString.split("/");

    let year = parseInt(dateArray[2]);
    let month = parseInt(dateArray[1], 10) - 1;
    let date = parseInt(dateArray[0]);

    var _entryDate = new Date(year, month, date);

    return _entryDate
  }

  getAge(birthday: Date): string {
    let timeDiff = Math.abs(Date.now() - birthday.getTime());
    let age = Math.floor((timeDiff / (1000 * 3600 * 24)) / 365.25);
    return age.toString();
  }

}
