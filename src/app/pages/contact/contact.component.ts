import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
// import { PROFILE } from 'src/app/models/user.info.const';
import { AlertService } from 'src/app/services/alert.service';
import { SendMailService } from 'src/app/services/send-mail.service';
import { UserService } from 'src/app/services/user.service';


@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit, OnDestroy {


  contact: Array<any> = new Array<any>();
  listObservers$: Array<Subscription> = [];
  loaded: boolean = false;

  sendEmailForm: FormGroup = new FormGroup({});
  disabledButton: boolean = false;
  nameInvalid: boolean = false;
  fromInvalid: boolean = false;
  subjectInvalid: boolean = false;
  messageInvalid: boolean = false;


  constructor(
    public user: UserService,
    public alertService: AlertService,
    private translate: TranslateService,
    private mailService: SendMailService,
  ) { }


  ngOnInit(): void {
    const observerContact$ = this.user.user$.subscribe((data: any) => {
      if (data !== null && data !== undefined) {
        this.contact = data.contact;
        this.createForm();
        this.loaded = true;
      }
    });
    this.listObservers$ = [observerContact$]
  }


  ngOnDestroy(): void {
    this.listObservers$.forEach(u => u.unsubscribe());
    this.loaded = false;
  }


  async sendEmail(): Promise<void> {
    if (this.sendEmailForm.valid) {
      this.disabledButton = true;

      this.errores();
      const { from, to, name, subject, message } = this.sendEmailForm.value;


      await this.mailService.sendMail({ from, to, name, subject, message }).toPromise()
        .then(() => {
          this.alertService.setTextAlert({
            text: this.translate.instant('contact.email_send.success'),
            type: 'success',
            timeOut: 4000
          });
          this.sendEmailForm.reset();
        })
        .catch(() => {
          this.alertService.setTextAlert({
            text: this.translate.instant('contact.email_send.error'),
            type: 'danger',
            timeOut: 6000
          });
        })
        .finally(() => {
          this.alertService.show();
          this.disabledButton = false;
        });

    }
    else {
      this.errores();
      this.disabledButton = false;
    }
  }


  private errores(): void {
    this.nameInvalid = !(this.sendEmailForm.controls.name.valid) ? true : false;
    this.fromInvalid = !(this.sendEmailForm.controls.from.valid) ? true : false;
    this.subjectInvalid = !(this.sendEmailForm.controls.subject.valid) ? true : false;
    this.messageInvalid = !(this.sendEmailForm.controls.message.valid) ? true : false;
  }


  private createForm(): void {

    const to_email = this.contact.find((i: any) => i.label === 'contact.email').value;

    this.sendEmailForm = new FormGroup(
      {
        name: new FormControl('', [Validators.required]),
        from: new FormControl('', [Validators.required, Validators.email]),
        to: new FormControl(to_email, [Validators.required, Validators.email]),
        subject: new FormControl('', [Validators.required]),
        message: new FormControl('', [Validators.required])
      }
    );
  }


}
