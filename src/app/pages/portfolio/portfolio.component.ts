import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { UserService } from 'src/app/services/user.service';


@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.scss']
})
export class PortfolioComponent implements OnInit, OnDestroy {


  portfolio: Array<any> = new Array<any>();
  listObservers$: Array<Subscription> = [];


  constructor(public user: UserService) { }


  ngOnInit(): void {
    const observerPortfolio$ = this.user.user$.subscribe((data: any) => {
      if (data !== null && data !== undefined) {
        this.portfolio = data.portfolio;
      }
    })
    this.listObservers$ = [observerPortfolio$]
  }


  ngOnDestroy(): void {
    this.listObservers$.forEach(u => u.unsubscribe());
  }


}
