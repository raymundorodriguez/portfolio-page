import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertService } from 'src/app/services/alert.service';
import { CommentService } from 'src/app/services/comment.service';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.scss']
})
export class CommentComponent implements OnInit {


  commentForm: FormGroup = new FormGroup({});
  nameInvalid: boolean = false;
  disabledButton: boolean = false;


  constructor(
    private _fb: FormBuilder,
    public alertService: AlertService,
    private comments: CommentService
  ) { }


  ngOnInit(): void {
    this.createForm();
  }

  private createForm(): void {
    this.commentForm = this._fb.group(
      {
        _id: [null],
        name: [null, Validators.required],
        position: [null, Validators.required],
        title: [null, Validators.required],
        description: [null, Validators.required],
      }
    );
  }


  async postComment(): Promise<void> {

    if (this.commentForm.invalid) {
      return;
    }

    this.disabledButton = true;

    this.comments.insertComment(this.commentForm.value).subscribe((data) => {
      this.disabledButton = false;

      if (data.inserted) {
        this.commentForm.reset();

        this.alertService.setTextAlert({
          text: "Muchas gracias por tu opinion, es muy importante para mi",
          type: 'success',
          timeOut: 4000
        });

        this.alertService.show();
      }
    });

  }


}
