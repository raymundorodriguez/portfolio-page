import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserService } from '../../services/user.service';
import { SendMailService } from '../../services/send-mail.service';
import { Subscription } from 'rxjs';
import { CustomModalService } from 'src/app/services/custom-modal.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {


  profile_name: string = '';
  profile_professions: string[] = [];
  listObservers$: Array<Subscription> = [];
  donwloadCVForm: FormGroup = new FormGroup({});
  emailInvalid: boolean = false;
  downloadText: string = '';


  constructor(
    public user: UserService,
    private modal: CustomModalService,
    private downloadCVService: SendMailService,
  ) { }


  ngOnInit(): void {
    this.createForm();
    const observerHome$ = this.user.user$.subscribe((data: any) => {
      if (data !== undefined && data !== null) {
        this.profile_name = data.short_name;
        this.profile_professions = data.professions;
      }
    });
    this.listObservers$ = [observerHome$];
  }


  ngOnDestroy(): void {
    this.listObservers$.forEach(u => u.unsubscribe());
  }


  createForm(): void {
    this.donwloadCVForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email]),
    });
  }


  openModal(): void {
    this.modal.open();
    this.donwloadCVForm.reset()
  }


  async downloadCV(): Promise<void> {
    if (this.donwloadCVForm.valid) {
      this.emailInvalid = true;
      this.errores();

      const { email } = this.donwloadCVForm.value;

      await this.downloadCVService.downloadCV(email).toPromise()
        .then((response: any) => {
          console.log(response);

          // Create dowload Link
          const a = document.createElement("a");
          a.href = './assets/cv_raymundo_rodriguez.pdf';
          a.setAttribute("download", 'raymundo_rodriguez.pdf');
          document.body.appendChild(a);
          a.click();
          document.body.removeChild(a);

          this.donwloadCVForm.reset();
          this.modal.close();
        });

    } else {
      this.errores();
      this.emailInvalid = false;
    }
  }


  private errores(): void {
    this.emailInvalid = !(this.donwloadCVForm.controls.email.valid) ? true : false;
  }

}
