import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { UserService } from 'src/app/services/user.service';


@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss']
})
export class ServicesComponent implements OnInit, OnDestroy {


  services: Array<any> = new Array<any>();
  listObservers$: Array<Subscription> = [];


  constructor(public user: UserService) { }


  ngOnInit(): void {
    const observerServices$ = this.user.user$.subscribe((data: any) => {
      if (data !== null && data !== undefined) {
        this.services = data.services;
      }
    });
    this.listObservers$ = [observerServices$]
  }


  ngOnDestroy(): void {
    this.listObservers$.forEach(u => u.unsubscribe())
  }


}
