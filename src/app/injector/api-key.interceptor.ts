import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable()
export class ApiKeyInterceptor implements HttpInterceptor {


  constructor() { }


  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    try {

      let newRequest = request

      newRequest = request.clone(
        {
          setHeaders: {
            'x-api-key': 'zaCELgL.0imfnc8mVLWwsAawjYr4Rx-Af50DDqtlx@',
          }
        }
      )

      return next.handle(newRequest);

    } catch (e) {
      console.log('🔴 Error => ', e)
      return next.handle(request);
    }
  }
}

