import { Component, HostListener } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ThemesService } from 'src/app/services/themes.service';


@Component({
  selector: 'style-switcher',
  templateUrl: './style-switcher.component.html',
  styleUrls: ['./style-switcher.component.scss']
})
export class StyleSwitcherComponent {


  open: boolean = false;
  isDark: boolean;
  lang: boolean = false;


  constructor(public theme: ThemesService, private translate: TranslateService) {
    this.isDark = this.theme.isDarkThemeFromUser();
    this.theme.setDarkTheme(this.isDark);
    this.translate.use('es');
  }


  @HostListener("window:scroll", ['$event'])
  onScrollScreen($event: Event) {
    if ($event.type === 'scroll') {
      this.open = false;
    }
  }


  @HostListener("window:mousewheel", ['$event'])
  onWheel($event: Event) {
    if ($event.type === 'mousewheel') {
      this.open = false;
    }
  }


  open_switcher(): void {
    this.open = !this.open;
  }


  setActiveStyle(color: string): void {
    this.theme.setColorTheme(color);
  }

  setDarkTheme(): void {
    this.isDark = !this.isDark;
    this.theme.setDarkTheme(this.isDark);
  }


  changeLang(): void {
    this.lang = !this.lang;

    if (this.lang) {
      this.translate.use('en');
    } else {
      this.translate.use('es');
    }
  }


}
