import { Component } from '@angular/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-aside',
  templateUrl: './aside.component.html',
  styleUrls: ['./aside.component.scss']
})
export class AsideComponent {


  open_aside: boolean = false;
  appVersion: string = environment.appVersion;

  constructor() { }


  showAsideSection(): void {
    this.open_aside = !this.open_aside;
  }


}
