import { AfterViewInit, Component, Input } from '@angular/core';
import { TranslateService } from "@ngx-translate/core";
import { Observable, from, interval, concat, of, BehaviorSubject } from "rxjs";
import { map, take, repeat, delay, concatMap, ignoreElements } from "rxjs/operators";


@Component({
  selector: 'typing-animation',
  templateUrl: './typing-animation.component.html',
  styleUrls: ['./typing-animation.component.scss']
})
export class TypingAnimationComponent implements AfterViewInit {


  private _data = new BehaviorSubject<string[]>([]);


  @Input() set titles(value: string[]) { this._data.next(value); };
  @Input() velocidad: number = 100;


  get titles() {
    return this._data.getValue();
  }

  value: string = '';


  constructor(private translate: TranslateService) { }


  ngAfterViewInit(): void {
    this._data.subscribe(x => {
      if (x.length > 0) {
        this.typing(x).subscribe(data => {
          this.value = data;
        });
      }
    });
  }


  type(word: string, speed: number, backwards: boolean = false): Observable<string> {
    return interval(speed).pipe(
      map(x =>
        backwards ? word.substr(0, word.length - x - 1) : word.substr(0, x + 1)
      ),
      take(word.length)
    );
  }


  typeEffect(word: string) {

    let _word: string = '';
    this.translate.get(word).subscribe((translated: string) => _word = translated);

    return concat(
      this.type(_word, this.velocidad), // type forwards
      of("").pipe(
        delay(1200),
        ignoreElements()
      ), // pause
      this.type(_word, this.velocidad, true), // delete
      of("").pipe(
        delay(300),
        ignoreElements()
      ) // pause
    );
  }


  typing(_titles: string[]): Observable<any> {
    return from(_titles).pipe(
      concatMap((word: string) => this.typeEffect(word)),
      repeat()
    );
  }


}
