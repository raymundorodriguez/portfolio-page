import { Component, OnInit } from '@angular/core';
import { AlertService } from 'src/app/services/alert.service';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit {


  text: string = '';
  type: string = '';
  icon: string = '';

  private _icon: Array<any> = [
    {
      type: '',
      icon: 'fas fa-terminal'
    },
    {
      type: 'success',
      icon: 'far fa-check-circle'
    },
    {
      type: 'warning',
      icon: 'fas fa-exclamation-triangle'
    },
    {
      type: 'danger',
      icon: 'fas fa-times-circle'
    },
    {
      type: 'info',
      icon: 'fas fa-exclamation-circle '
    }
  ];


  constructor(public alertService: AlertService) { }


  ngOnInit(): void {
    this.alertService.alert$.subscribe((_alert: any) => {
      this.text = _alert.text;
      this.type = _alert.type;
      this.icon = this._icon.find(ic => ic.type === _alert.type).icon;
    });
  }


}
