import { Component } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { slideInAnimation } from '../../route-animation';


@Component({
  selector: 'app-main-container',
  templateUrl: './main-container.component.html',
  styleUrls: ['./main-container.component.scss'],
  animations: [slideInAnimation]
})
export class MainContainerComponent {


  constructor(public user: UserService) {
    this.user.getUser();
  }


}
