import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { CustomModalService } from 'src/app/services/custom-modal.service';


@Component({
  selector: 'custom-modal',
  templateUrl: './custom-modal.component.html',
  styleUrls: ['./custom-modal.component.scss']
})
export class CustomModalComponent implements OnInit {


  display$: Observable<'open' | 'close'>;


  constructor(private modalService: CustomModalService) {
    this.display$ = new Observable();
  }


  ngOnInit() {
    this.display$ = this.modalService.watch();
  }


  close() {
    this.modalService.close();
  }


}
