export interface Comment {
    _id: string;
    name: string;
    position: string;
    title: string;
    description: string;
}
