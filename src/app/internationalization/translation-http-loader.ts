import { HttpClient } from "@angular/common/http";
import { TranslateLoader } from "@ngx-translate/core";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";


export class TranslationHttpLoader implements TranslateLoader {


    private readonly ENDPOINT = environment.api;

    constructor(private httpClient: HttpClient) { }

    /** * Gets the translations from the server */
    public getTranslation(lang: string): Observable<Object> {

        if (lang == null) {
            lang == "es";
        }

        const urls = {
            es: `${this.ENDPOINT}/user/get-user?lang=es-MX`,
            en: `${this.ENDPOINT}/user/get-user?lang=en-US`
        };


        let observer = new Observable<Object>(observer => {
            this.httpClient.get<Object>(urls[lang as keyof typeof urls])
                .subscribe(
                    {
                        next: (response: any) => {
                            let { data } = response;
                            data.langs.translates.home.info = this.experienceYears(data.langs.translates.home.info);
                            observer.next(data.langs.translates);
                            observer.complete();
                        },
                        error: (error: any) => {
                            // console.log(error);
                        }
                    }
                );
        });

        return observer;
    }

    public experienceYears(text: string): string {

        let experience: Date = new Date("July 01, 2019 09:00:00");
        var ageDifMs = Date.now() - experience.getFullYear();
        var ageDate = new Date(ageDifMs); // miliseconds from epoch
        let result = Math.abs(ageDate.getUTCFullYear() - 2019).toString();

        return text.replace("{{experience_years}}", result);
    }

}

