export const environment = {
  production: true,
  appVersion: require('../../package.json').version,
  api: 'https://api-rjrm.vercel.app/api'
};
